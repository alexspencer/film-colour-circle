# Author: Alex Spencer
# Description: Uses film-grab website to grab a film still/title and present the main
#              colours in each still using circlize in R
# Clustering source: https://www.r-bloggers.com/r-k-means-clustering-on-an-image/
# Film-grab site: https://film-grab.com

# *****************************************************************************************
#                       Film still colour analysis circle plot                                                           
# *****************************************************************************************

library(rvest)
library(jsonlite)
library(jpeg)
library(png)
library(tools)
library(ggplot2)
library(circlize)
library(imager)

# *****************************************************************************************
#                            Fucntion to the film name and image                                                       
# *****************************************************************************************

# For the given URL, find the film name and the URL for the film image, then download
getImageFromURL <- function(url) {
  image_details = {}
  
  # Get webpage
  webpage <- html_session(url)
  image_details$film_name <- webpage %>% html_nodes("h1.entry-title") %>% html_text() # Film name
  print(paste("Downloading image for", image_details$film_name))
  
  # Filter for the large thumbnail image
  img.url <- webpage %>% html_nodes("img.attachment-large-thumb") %>% html_attr("src")
  
  # Download and read image
  tempfile <- tempfile()
  download.file(img.url, tempfile, mode = "wb")
  if (grepl(".png", img.url, ignore.case = TRUE)) {
    image_details$image <- readPNG(tempfile)
  }
  else {
    image_details$image <- readJPEG(tempfile)  
  }
  
  file.remove(tempfile)
  return(image_details)
}

# *****************************************************************************************
#                          Function to reduce image colour depth                                                        
# *****************************************************************************************

# Turns a raster image into x, y type records
xyFromImg <- function(img) {
  imgDm <- dim(img)
  
  # Assign RGB channels to data frame
  if (length(imgDm) == 3) {
    imgRGB <- data.frame(
      x = rep(1:imgDm[2], each = imgDm[1]),
      y = rep(imgDm[1]:1, imgDm[2]),
      R = as.vector(img[,,1]),
      G = as.vector(img[,,2]),
      B = as.vector(img[,,3])
    )
  }
  else {
    imgRGB <- data.frame(
      x = rep(1:imgDm[2], each = imgDm[1]),
      y = rep(imgDm[1]:1, imgDm[2]),
      R = as.vector(img[,]),
      G = as.vector(img[,]),
      B = as.vector(img[,])
    )
  }
  
  return(imgRGB)
}

# Uses clustering to find n colours to use in an image
reduceImageColourDepth <- function(img, kColours = 4) {
  newImage = {}
  
  # Assign RGB channels to data frame
  imgRGB <- xyFromImg(img)
  
  # Perform clustering
  kMeans <- kmeans(imgRGB[, c("R", "G", "B")], centers = kColours)
  newImage$colours <- rgb(kMeans$centers)
  newImage$imageRGB <- rgb(kMeans$centers[kMeans$cluster,])
  
  return(newImage)
}

# *****************************************************************************************
#                                 Rescale image function                                                
# *****************************************************************************************

rescaleImage <- function(image, desired_image_width) {
  image_temp <- image %>% as.cimg
  image_width <- height(image_temp) # The image is rotated, use "height" for the width
  scale_proportion <- desired_image_width / image_width
  scale_proportion <- -1 * (1 - scale_proportion) * 100
  
  new_image <- imager::resize(image_temp, scale_proportion, scale_proportion) %>% as.array
  
  new_image <- new_image[,,1,] # Drop the time dimension, not needed
  return(new_image)
}

# *****************************************************************************************
#                                   Main code                                               
# *****************************************************************************************

number_films <- 6   # Change this if you want
number_colours <- 6 # Change this if you want

# Grab n random screenshots
images <- replicate(number_films, getImageFromURL("https://film-grab.com/?random"), simplify = FALSE)

# Or you can give a list of films on the film-grab site
#fixed_urls <- c("https://film-grab.com/2012/11/05/amelie/",
#                "https://film-grab.com/2017/09/13/gladiator/",
#                "https://film-grab.com/2017/07/07/a-bigger-splash/",
#                "https://film-grab.com/2017/06/08/petes-dragon/")

#images <- lapply(fixed_urls, getImageFromURL)

# *****************************************************************************************
#                                 Prepare plotting data                                                
# *****************************************************************************************

film_names = unlist(lapply(images, `[[`, 1)) # Extract the film names from the list
film_images = lapply(images, `[[`, 2)        # Extract the film images from the list

# Determine film still colours
film_colours <- lapply(film_images, reduceImageColourDepth, kColours = number_colours)
film_colours <- lapply(film_colours, `[[`, 1) # Ignore the image, just get the colours
original_par = par()

# *****************************************************************************************
#                                    Parameters 
# *****************************************************************************************

# Parameters / adjustable settings
gap_width <- 0.02 # Gap width between colour wedges
picture_percent <- 0.5 # What percentage of the available height should the picture take up?
colour_percent <- 0.3  # What percentage of the available height should the colour band take up?
# Does not have to add up to 1.0, but must be <= 1.0
screenshot_scaling <- 1 # Scaling the image to a circle takes time, in debug run this around 0.05
# in final run, can change this to above 0.4 to get good resolution
# does NOT affect colour extraction

png_width <- 1080      # Size of the png file

# *****************************************************************************************
#                              Plot circle plot with films                                                    
# *****************************************************************************************

# Calculate the desired image width
desired_image_width <- round((png_width * 0.9 * pi) / number_films)

# Rescale images and use these instead
rescaled_film_images <- sapply(film_images, rescaleImage, desired_image_width)

png(filename = "film_colours.png", width = png_width, height = png_width, res = 300) # Prepare writing to file

# Initial plot of the circle
circos.par("cell.padding" = c(0, 0, 0, 0))
circos.initialize(film_names, xlim = c(0, 1))

# Add the film image and title
circos.track(ylim = c(0, 1), panel.fun = function(x, y) {
  # Select details of current sector
  name = get.cell.meta.data("sector.index")
  i = get.cell.meta.data("sector.numeric.index")
  xlim = get.cell.meta.data("xlim")
  ylim = get.cell.meta.data("ylim")
  
  img = rescaled_film_images[[i]]
  circos.raster(img, CELL_META$xcenter, CELL_META$ycenter, 
                width = CELL_META$xrange, height = CELL_META$yrange, 
                facing = "bending.inside", scaling = screenshot_scaling, niceFacing = TRUE)
  
  # Add on film title
  circos.text(x = mean(xlim), y = 1.1, labels = name, facing = "bending.inside", cex = 0.6, niceFacing = TRUE)
}, track.height = picture_percent, bg.border = NA)

# Add the colour banding
circos.track(ylim = c(0, 1), panel.fun = function(x, y) {
  # Select details of current sector
  i = get.cell.meta.data("sector.numeric.index")
  
  # Try plotting one big rectangle first
  rectangle_width <- (1.0 - (gap_width * (number_colours - 1))) / number_colours
  for (colID in seq(1, number_colours)) {
    minX <- (rectangle_width + gap_width) * (colID - 1)
    maxX <- minX + rectangle_width
    circos.polygon(x = c(minX, maxX, maxX, minX, minX), y = c(0, 0, 1, 1, 0), col = film_colours[[i]][colID], border = NA)
  }
}, track.height = colour_percent, bg.border = NA)
circos.clear()
dev.off()

# Copy file to shared
file.copy(from = "film_colours.png", to = "/media/sf_Shared/film_colours.png", overwrite = TRUE)