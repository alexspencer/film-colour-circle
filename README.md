# Film Colour Circle

<html>
<body>
<p><img src="https://gitlab.com/alexspencer/film-colour-circle/raw/master/film_colours.png" alt="Film Colour Circle" width="600" height="600" align="middle"></p>
</body>
</html>

## Description

This R code creates a film colour wheel using the circlize package. Film stills are obtained from [film-grab.com](https://film-grab.com) via the [random](https://film-grab.com/?random) page. Thank you [@filmgrabber](https://twitter.com/filmgrabber) for the great site and permission to post this.

## Use

This was run in RStudio. Install required packages. Run code - feel free to change the number of films / colours. The `screenshot_scaling` parameter is useful for testing, setting it to 0.1 means the circle renders quickly but the resolution of the screenshot will be low. Try specifying your own films by uncommenting the required section.

## Issues

- [ ] It's quite slow
- [ ] There is some aliasing going on with the colour swatches - rendering at double the resolution and displaying at 50% could resolve this?
- [ ] No guarantee for good results in terms of interesting colours
- [ ] NSFW risk! The films are RANDOM and the screenshots could be NSFW